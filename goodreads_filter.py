import requests
from bs4 import BeautifulSoup
import pprint 
import json
from pathlib import Path
import os.path
import collections
import xlrd

ConfigFilename = 'in/config.json'
CurrentCookieFilename = 'in/current_cookie.txt'
ResponcesFolder = 'out/page_responses'
FilteredPagesFolder = 'out/filtered_pages'
# for debug
PrintUrlPages = True

def main():
    config = json.loads(Path(ConfigFilename).read_text())

    filtered_pages, filtered_negative_authors = filter_authors(config)

    shelf_filtered_folder = f'{FilteredPagesFolder}/{config["shelf_name"]}'
    Path(shelf_filtered_folder).mkdir(parents=True, exist_ok=True)

    page_index = config["page_start"]
    for page in filtered_pages:
        Path(f'{shelf_filtered_folder}/{page_index}.html').write_text(page, encoding="utf-8")
        page_index += 1


def filter_authors(config):
    filtered_pages=[]
    filtered_negative_authors = []

    current_cookie = None
    with open(CurrentCookieFilename, "r", encoding="utf-8") as f:
        current_cookie = f.readline()

    headers = {'Cookie': current_cookie}
    ignored_authors = parse_ignored_authors(config['ignored_authors_xlsx'])
    # todo: check for duplicates
    # counter = collections.Counter(a).items()
    # print([item for item, count in  if count > 1])

    for page in range(config['page_start'], config['page_end'] + 1):
        cache_dir = f'{ResponcesFolder}/{config["shelf_name"]}'
        cache_filename = cache_dir + f'/page_{page}.txt'
        html = None
        if os.path.isfile(cache_filename):
            print(f'using cache for page {page}')
            html = Path(cache_filename).read_text(encoding="utf-8")
        else:
            print(f'requesting page {page}')
            r = requests.get(f'https://www.goodreads.com/shelf/show/{config["shelf_name"]}?page={page}', headers=headers)
            print(f'recieved page {page}')
            html = r.text

        soup = BeautifulSoup(html, 'html.parser')

        if not os.path.isfile(cache_filename):
            Path(cache_dir).mkdir(parents=True, exist_ok=True)
            Path(cache_filename).write_text(soup.prettify(), encoding="utf-8")

        book_element_lists = soup.find_all(class_="elementList")

        page_filtered_negative = []
        for element in book_element_lists:

            author_tag = element.find(class_="authorName")
            if not author_tag:
                continue

            author = author_tag.text.strip()
            if (author in ignored_authors):
                element.decompose()
                page_filtered_negative.append(author)
                continue
            
            title_tag = element.find(class_="bookTitle")
            relative_url = title_tag.get('href')
            absolute_url = f'https://www.goodreads.com/{relative_url}'
            # replacing by string since it occurs multiple times
            all_hrefs = element.findAll(href=relative_url)
            for h in all_hrefs:
                h['href'] = absolute_url

        filtered_pages.append(str(soup))
        filtered_negative_authors.append(page_filtered_negative)

    return filtered_pages, filtered_negative_authors


def parse_ignored_authors(xlsx_filename):
    workbook = xlrd.open_workbook(xlsx_filename, on_demand = True)
    sheet = workbook.sheet_by_index(0)
    ignored_authors_col = map(lambda cell : cell.value, sheet.col(1))
    return list(filter(lambda author: author, ignored_authors_col))


if __name__ == '__main__':
    main()

# print unique authors (to easily see who to ignore next)
# print('---------Unique non-ignored authors-------------')
# authors = map(lambda book: book.author_name, filtered_positive)
# s = pprint.pformat(set(authors))
# s = s.replace("\'", "\"")
# print(s)
